class CreateIpBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :ip_blocks do |t|
      t.integer :ip, null: false , :limit => 8
      t.timestamps
    end
  end
end
