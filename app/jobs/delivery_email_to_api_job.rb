class DeliveryEmailToApiJob < ApplicationJob
  queue_as :default

  def perform(uuid, ip, user_agent)
    post_fields = { uuid: uuid, ip: ip, user_agent: user_agent }
    site = RestClient::Resource.new(ApplicationJob::APIService)
    begin
      tries ||= 5
      site['/api/v1/send_themes'].post post_fields.to_query
    rescue RestClient::ExceptionWithResponse => e
      tries -= 1
      sleep 60
      if tries > 0
        retry
      else
        attachments = {}
        wording = "uuid:#{uuid}\n"
        wording += "IP:#{ip}\n"
        wording += "UserAgent: #{user_agent}"
        attachments.store(:color, 'danger')
        attachments.store(:text, wording)
        attachments.store(:author_name, 'Call Deliver API Error!')
        # TODO before deploy you should edit channel and theme server name
        SlackNotifyJob.perform_later(attachments, 'channel', 'theme_server_name')
        Rails.logger.info("Call Deliver API Retry Fail: #{post_fields}")
      end
    end
  end
end
