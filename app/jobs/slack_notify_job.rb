class SlackNotifyJob < ApplicationJob
  queue_as :default
# TODO before deploy you should edit channel and theme server name
  def perform(attachments, channel = 'channel_name', username = 'theme_server_name')
    if Rails.env.production?
      notifier = Slack::Notifier.new '',
                                     channel: channel,
                                     username: username
      notifier.ping '', attachments: [attachments]
    else
      Rails.logger.info "Nofity slack: #{attachments} (at #{channel} by #{username})"
    end
  end
end
