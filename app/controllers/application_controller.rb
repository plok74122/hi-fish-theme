class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def record_ip
    IpBlock.add_access_log!(request.remote_ip)
  end

  def ip_block
    unless IpBlock.can_access?(request.remote_ip)
      redirect_to IpBlock::RedriectUrl
      return
    end
  end
end
