class WelcomeController < ApplicationController
  before_action :check_params, only: [:email_logger, :second_email]

  def email_logger
    SendDetailToApiJob.perform_later(:params[:uuid],
                                     request.remote_ip,
                                     request.user_agent)
    render inline: ''
  end

  def deliver_email
    DeliveryEmailToApiJob.perform_later(:params[:uuid],
                                        request.remote_ip,
                                        request.user_agent)
    render inline: ''
  end

  private

  def check_params
    if params.has_key?(:uuid)
      begin
        EncryptService.decrypt(params[:uuid])
      rescue
        raise
      end
    else
      render inline: '', status: 400
      return
    end
  end
end
