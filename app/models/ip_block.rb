class IpBlock < ApplicationRecord
  RedriectUrl = ""

  def self.add_access_log!(ip)
    self.create(:ip => IPAddr.new(ip).to_i)
  end

  def self.can_access?(ip)
    exist_records = self.where(:ip => (IPAddr.new(ip).to_i - 255)..(IPAddr.new(ip).to_i + 255), :created_at => (Time.now - 3.hours)..(Time.now))
    exist_records.count > 3 ? false : true
  end
end
